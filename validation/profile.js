const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateProfileInput(data) {
    let errors = {};

    data.handle = !isEmpty(data.handle) ? data.handle : '';
    data.status = !isEmpty(data.status) ? data.status : '';
    data.interests = !isEmpty(data.interests) ? data.interests : '';

    if (!Validator.isLength(data.handle, { min: 2, max: 40 })) {
        errors.handle = 'Handle must be between 2 and 4 characters.';
    }

    if (Validator.isEmpty(data.handle)) {
        errors.handle = 'A profile handle is required.';
    }

    if (Validator.isEmpty(data.status)) {
        errors.status = 'Status field is required.';
    }

    if (Validator.isEmpty(data.interests)) {
        errors.interests = 'Interests field is required.';
    }

    if (!isEmpty(data.website)) {
        if (!Validator.isURL(data.website)) {
            errors.website = 'That doesn\'t appear to be a valid URL.';
        }
    }

    if (!isEmpty(data.youtube)) {
        if (!Validator.isURL(data.youtube)) {
            errors.youtube = 'That doesn\'t appear to be a valid URL.';
        }
    }

    if (!isEmpty(data.twitter)) {
        if (!Validator.isURL(data.twitter)) {
            errors.twitter = 'That doesn\'t appear to be a valid URL.';
        }
    }

    if (!isEmpty(data.facebook)) {
        if (!Validator.isURL(data.facebook)) {
            errors.facebook = 'That doesn\'t appear to be a valid URL.';
        }
    }

    if (!isEmpty(data.linkedin)) {
        if (!Validator.isURL(data.linkedin)) {
            errors.linkedin = 'That doesn\'t appear to be a valid URL.';
        }
    }

    if (!isEmpty(data.instagram)) {
        if (!Validator.isURL(data.instagram)) {
            errors.instagram = 'That doesn\'t appear to be a valid URL.';
        }
    }

    return {
        errors: errors,
        isValid: isEmpty(errors)
    };
};