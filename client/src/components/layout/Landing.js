import React, { Component } from 'react'

class Landing extends Component {
  render() {
    return (
        <div className="landing">
            <div className="dark-overlay">
                <div className="landing-inner">
                    <h1>The social network for finance nerds</h1>

                    <a href="register.html" className="button-big">Join Social Finance</a>
                </div>
            </div>
        </div>
    )
  }
}

export default Landing;