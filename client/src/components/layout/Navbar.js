import React, { Component } from 'react'

class Navbar extends Component {
  render() {
    return (
        <nav className="navigation">
            <a className="nav-brand" href="/"><i className="fas fa-leaf"></i></a>
            <a className="nav-link" href="/">Directory</a>
            <div className="nav-account">
                <a className="nav-link login" href="/">Login</a>
                <a className="nav-link sign-up" href="/">Sign Up</a>
            </div>
        </nav>
    )
  }
}

export default Navbar;