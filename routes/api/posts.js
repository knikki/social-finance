const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

// Load validation for post input.
const validatePostInput = require('../../validation/post');

const Post = require('../../models/Post');
const Profile = require('../../models/Profile');

// @route   GET api/posts
// @desc    Get all posts.
// @access  Public
router.get('/', (req, res) => {
    Post.find()
        .sort({ date: -1 })
        .then(posts => res.json(posts))
        .catch(error => res.status(404).json({ noposts: 'No posts found.' }));
});

// @route   GET api/posts/:id
// @desc    Get post by id.
// @access  Public
router.get('/:id', (req, res) => {
    Post.findById(req.params.id)
        .then(post => res.json(post))
        .catch(error => res.status(404).json({ nopost: 'No post found.' }));
});

// @route   POST api/posts
// @desc    Create a new post.
// @access  Private
router.post('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    const { errors, isValid } = validatePostInput(req.body);

    // Check input validation.
    if (!isValid) {
        return res.status(400).json(errors);
    }

    const newPost = new Post({
        text: req.body.text,
        name: req.body.name,
        avatar: req.body.avatar,
        user: req.user.id
    });

    newPost.save().then(post => res.json(post));
});

// @route   DELETE api/posts/:id
// @desc    Delete post.
// @access  Private
router.delete('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    Profile.findOne({ user: req.user.id })
        .then(profile => {
            Post.findById(req.params.id)
                .then(post => {
                    // Check to make sure user is the owner of the post.
                    if (post.user.toString() !== req.user.id) {
                        return res.status(401).json({ notauthorized: 'User not authorized' });
                    }
                    // Delete the post.
                    post.remove().then(() => res.json({ success: true }));
                })
                .catch(error => res.status(404).json({ nopost: 'No post found.' }));
        });
});

// @route   POST api/posts/like/:id
// @desc    Like a post.
// @access  Private
router.post('/like/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    Profile.findOne({ user: req.user.id })
        .then(profile => {
            Post.findById(req.params.id)
                .then(post => {
                    // Check to see if the user has already liked the post.
                    if (post.likes.filter(like => like.user.toString() === req.user.id).length > 0) {
                        return res.status(400).json({ alreadyliked: 'User has already liked this post.' });
                    }
                    // Like the post (add user id to likes array) and save it.
                    post.likes.unshift({ user: req.user.id });
                    post.save().then(post => res.json(post));
                })
                .catch(error => res.status(404).json({ nopost: 'No post found.' }));
        });
});

// @route   POST api/posts/unlike/:id
// @desc    Remove like from a post.
// @access  Private
router.post('/unlike/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    Profile.findOne({ user: req.user.id })
        .then(profile => {
            Post.findById(req.params.id)
                .then(post => {
                    // Check to see if user hasn't yet liked the post.
                    if (post.likes.filter(like => like.user.toString() === req.user.id).length === 0) {
                        return res.status(400).json({ notliked: 'User has not yet liked this post.' });
                    }
                    // Remove like from post and save.
                    const indexToRemove = post.likes.map(item => item.user.toString()).indexOf(req.user.id);
                    post.likes.splice(indexToRemove, 1);
                    post.save().then(post => res.json(post));
                })
                .catch(error => res.status(400).json({ nopost: 'No post found.' }));
        });
});

// @route   POST api/posts/comment/:id
// @desc    Add a comment to a post.
// @access  Private

router.post('/comment/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    const { errors, isValid } = validatePostInput(req.body);

    if (!isValid) {
        return res.status(400).json(errors);
    }

    Post.findById(req.params.id)
        .then(post => {
            const newComment = {
                text: req.body.text,
                name: req.body.name,
                avatar: req.body.avatar,
                user: req.user.id
            };

            // Add comment to the comments array and save.
            post.comments.unshift(newComment);
            post.save().then(post => res.json(post));
        })
        .catch(error => res.status(404).json({ nopost: 'No post found.' }));
});

// @route   DELETE api/posts/comment/:id/:comment_id
// @desc    Remove a comment from a post.
// @access  Private

router.delete('/comment/:id/:comment_id', passport.authenticate('jwt', { session: false }), (req, res) => {
    Post.findById(req.params.id)
        .then(post => {
            // Check to make sure user is the owner of the comment.
            if (post.comments.filter(comment => comment.user.toString() === req.user.id).length === 0) {
                return res.status(401).json({ notauthorized: 'User not authorized' });
            } else {
                // Check to see if the comment exists.
                if (post.comments.filter(comment => comment._id.toString() === req.params.comment_id).length === 0) {
                    return res.status(404).json({ nocomment: 'Comment does not exist.' });
                }

                // Remove comment from post and save.
                const indexToRemove = post.comments.map(item => item._id.toString()).indexOf(req.params.comment_id);
                post.comments.splice(indexToRemove, 1);
                post.save().then(post => res.json(post));
            }
        })
        .catch(error => res.status(404).json({ nopost: 'No post found.' }));
});

module.exports = router;