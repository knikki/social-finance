const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

// Load validation for profile, experience, and education input.
const validateProfileInput = require('../../validation/profile');
const validateExperienceInput = require('../../validation/experience');
const validateEducationInput = require('../../validation/education');

// Load profile and user models.
const Profile = require('../../models/Profile');
const User = require('../../models/User');

// @route   GET api/profile
// @desc    Profile for the current user.
// @access  Private
router.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    const errors = {};

    Profile.findOne({ user: req.user.id })
        .then(profile => {
            if (!profile) {
                errors.noprofile = 'No profile for this user exists.'
                return res.status(404).json(errors);
            }
            res.json(profile);
        })
        .catch(error => res.status(404).json(error));
});

// @route   GET api/profile/all
// @desc    Get all profiles.
// @access  Public

router.get('/all', (req, res) => {
    const errors = {};

    Profile.find()
        .populate('user', ['name', 'avatar'])
        .then(profiles => {
            if (!profiles) {
                errors.noprofile = 'There are no profiles to display.';
                return res.status(404).json(errors);
            }
            res.json(profiles);
        })
        .catch(error => res.status(404).json({ profile: 'There are no profiles to display.'}));
});

// @route   GET api/profile/handle/:handle
// @desc    Get profile by handle.
// @access  Public

router.get('/handle/:handle', (req, res) => {
    const errors = {};

    Profile.findOne({ handle: req.params.handle })
        .populate('user', ['name', 'avatar'])
        .then(profile => {
            if (!profile) {
                errors.noprofile = 'No profile exists for this user.';
                return res.status(404).json(errors);
            }
            res.json(profile);
        })
        .catch(error => res.status(404).json({ profile: 'No profile exists for this user.' }));
});

// @route   GET api/profile/user/:user_id
// @desc    Get profile by user id.
// @access  Public

router.get('/user/:user_id', (req, res) => {
    const errors = {};

    Profile.findOne({ user: req.params.user_id })
        .populate('user', ['name', 'avatar'])
        .then(profile => {
            if (!profile) {
                errors.noprofile = 'No profile exists for this user.';
                return res.status(404).json(errors);
            }
            res.json(profile);
        })
        .catch(error => res.status(404).json({ profile: 'No profile exists for this user.' }));
});

// @route   POST api/profile
// @desc    Create or edit user profile.
// @access  Private
router.post('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    const { errors, isValid } = validateProfileInput(req.body);

    // Check input validation.
    if (!isValid) {
        return res.status(400).json(errors);
    }

    // Get fields.
    const profileFields = {};

    profileFields.user = req.user.id;

    if (req.body.handle) profileFields.handle = req.body.handle;
    if (req.body.company) profileFields.company = req.body.company;
    if (req.body.website) profileFields.website = req.body.website;
    if (req.body.location) profileFields.location = req.body.location;
    if (req.body.status) profileFields.status = req.body.status;

    // Interests
    if (typeof req.body.interests !== undefined) {
        profileFields.interests = req.body.interests.split(',');
    }

    if (req.body.bio) profileFields.bio = req.body.bio;
    if (req.body.redditname) profileFields.redditname = req.body.redditname;

    // Social media accounts.
    profileFields.social = {};
    if (req.body.youtube) profileFields.social.youtube = req.body.youtube;
    if (req.body.twitter) profileFields.social.twitter = req.body.twitter;
    if (req.body.facebook) profileFields.social.facebook = req.body.facebook;
    if (req.body.linkedin) profileFields.social.linkedin = req.body.linkedin;
    if (req.body.instagram) profileFields.social.instagram = req.body.instagram;

    Profile.findOne({ user: req.user.id })
        .then(profile => {
            if (profile) {
                // Update the profile.
                Profile.findOneAndUpdate(
                    { user: req.user.id },
                    { $set: profileFields },
                    { new: true }
                ).then(profile => res.json(profile));
            } else {
                // Check to see if the handle exists.
                Profile.findOne({ handle: profileFields.handle })
                    .then(profile => {
                        if (profile) {
                            errors.handle = "That handle is already in use.";
                            res.status(400).json(errors);
                        }
                        // Save profile.
                        new Profile(profileFields).save().then(profile => res.json(profile));
                    });
            }
        });
});

// @route   POST api/profile/experience
// @desc    Add experience to profile.
// @access  Private
router.post('/experience', passport.authenticate('jwt', { session: false }), (req, res) => {
    const { errors, isValid } = validateExperienceInput(req.body);

    // Check input validation.
    if (!isValid) {
        return res.status(400).json(errors);
    }

    Profile.findOne({ user: req.user.id })
        .then(profile => {
            const newExperience = {
                title: req.body.title,
                company: req.body.company,
                location: req.body.location,
                from: req.body.from,
                to: req.body.to,
                current: req.body.current,
                description: req.body.description
            };

            // Add to the beginning of the experience array in profile.
            profile.experience.unshift(newExperience);
            profile.save().then(profile => res.json(profile));
        });
});

// @route   POST api/profile/education
// @desc    Add education to profile.
// @access  Private
router.post('/education', passport.authenticate('jwt', { session: false }), (req, res) => {
    const { errors, isValid } = validateEducationInput(req.body);

    // Check input validation.
    if (!isValid) {
        return res.status(400).json(errors);
    }

    Profile.findOne({ user: req.user.id })
        .then(profile => {
            const newEducation = {
                school: req.body.school,
                degree: req.body.degree,
                location: req.body.location,
                from: req.body.from,
                to: req.body.to,
                current: req.body.current,
                description: req.body.description
            };

            // Add to the beginning of the education array in profile.
            profile.education.unshift(newEducation);
            profile.save().then(profile => res.json(profile));
        });
});

// @route   DELETE api/profile/experience/:exp_id
// @desc    Delete an experience from profile.
// @access  Private
router.delete('/experience/:exp_id', passport.authenticate('jwt', { session: false }), (req, res) =>  {
    Profile.findOne({ user: req.user.id })
        .then(profile => {
            // Get the index for removal.
            const indexToRemove = profile.experience.map(item => item.id).indexOf(req.params.exp_id);

            // Splice out of the array and then save profile.
            profile.experience.splice(indexToRemove, 1);
            profile.save().then(profile => res.json(profile));
        })
        .catch(error => res.status(404).json(error));
});

// @route   DELETE api/profile/education/:edu_id
// @desc    Delete an education from profile.
// @access  Private
router.delete('/education/:edu_id', passport.authenticate('jwt', { session: false }), (req, res) =>  {
    Profile.findOne({ user: req.user.id })
        .then(profile => {
            const indexToRemove = profile.education.map(item => item.id).indexOf(req.params.edu_id);
            profile.education.splice(indexToRemove, 1);
            profile.save().then(profile => res.json(profile));
        })
        .catch(error => res.status(404).json(error));
});

// @route   DELETE api/profile
// @desc    Delete user and profile.
// @access  Private
router.delete('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    Profile.findOneAndDelete({ user: req.user.id })
        .then(() => {
            User.findOneAndDelete({ user: req.user.id })
            .then(() => res.json({ success: true }));
        })
        .catch(error => res.status(404).json(error));
});

module.exports = router;