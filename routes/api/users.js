const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');

// Load validation for registration and login input.
const validateRegisterInput = require('../../validation/register');
const validateLoginInput = require('../../validation/login');

const keys = require('../../config/keys');
const User = require('../../models/User');

// @route   GET api/users/test
// @desc    Simple test route for users.
// @access  Public
router.get('/test', (req, res) => res.json({message: "Users is ready to roll."}));

// @route   POST api/users/register
// @desc    User registration.
// @access  Public
router.post('/register', (req, res) => {
    const { errors, isValid } = validateRegisterInput(req.body);

    // Check input validation.
    if (!isValid) {
        return res.status(400).json(errors);
    }

    User.findOne({ email: req.body.email })
        .then(user => {
            if (user) {
                errors.email = 'An account with this email address already exists.';
                return res.status(400).json(errors);
            } else {
                const avatar = gravatar.url(req.body.email, { s: '200', r: 'pg', d: 'mm' }); // Sending parameters for size, rating, and default.

                const newUser = new User({
                    name: req.body.name,
                    email: req.body.email,
                    password: req.body.password,
                    avatar: avatar
                });

                bcrypt.genSalt(10, (error, salt) => {
                    bcrypt.hash(newUser.password, salt, (error, hash) => {
                        if (error) throw error;
                        newUser.password = hash;
                        newUser.save()
                               .then(user => res.json(user))
                               .catch(error => console.log(error));
                    });
                });
            }
        });
});

// @route   POST api/users/login
// @desc    User login and JSON Web Token return.
// @access  Public
router.post('/login', (req, res) => {
    const { errors, isValid } = validateLoginInput(req.body);

    // Check input validation.
    if (!isValid) {
        return res.status(400).json(errors);
    }

    const email = req.body.email;
    const password = req.body.password;

    // Find the user by email.
    User.findOne({email})
        .then(user => {
            // Check for the user.
            if (!user) {
                errors.email = 'This user could not be found.';
                return res.status(404).json(errors);
            }
            // Check plain text password against hashed password using bcrypt.
            bcrypt.compare(password, user.password)
                .then(isMatch => {
                    if (isMatch) {
                        const payload = { id: user.id, name: user.name, avatar: user.avatar };
                        // Sign the token.
                        jwt.sign(payload, keys.secretOrKey, { expiresIn: 36000 }, (error, token) => {
                            res.json({ success: true, token: 'Bearer ' + token });
                        });
                    } else {
                        errors.password = 'You have entered an invalid password.';
                        return res.status(400).json(errors);
                    }
                });
        });
});

// @route   GET api/users/current
// @desc    Return the current user.
// @access  Private
router.get('/current', passport.authenticate('jwt', { session: false }), (req, res) => {
    res.json({
        id: req.user.id,
        name: req.user.name,
        email: req.user.email
    });
});

module.exports = router;