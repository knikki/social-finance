# Social Finance

This project is a **work in progress**. The back end API is complete, with endpoints for all of the basic tasks a user would want in a 'social network'. These include:

* Register a new account and login
* Create and update a user profile (using Gravatar for avatars)
* Delete account and profile
* Make new text posts
* Comment on and like posts

Validation is present for all user input.

Currently working on the front end. As of very early Wednesday 2/13 there are two simple components for a navigation bar and a landing page. These aren't hooked up yet, but I'm including a screenshot here because pictures are fun.

![alt text](https://i.imgur.com/F821Jbn.jpg "Landing page screenshot")

## TODO

Next up on the list is setting up React Router and component state, and then implementing Redux and authentication.

## Installation

To run both the server and client concurrently, use:

```bash
npm run dev
```